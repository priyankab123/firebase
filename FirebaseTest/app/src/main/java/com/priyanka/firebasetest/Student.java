package com.priyanka.firebasetest;

import com.google.firebase.database.IgnoreExtraProperties;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Student implements Serializable{

   public String Name;
   public int Age;
   public String MobileNum;
   public String College;
   public String DateAndTime;

    public Student()
    {

    }

    public Student(String name, int age, String mobileNum, String college,String dateAndTime)
    {
        this.Name = name;
        this.Age = age;
        this.MobileNum = mobileNum;
        this.College=college;
        this.DateAndTime=dateAndTime;
    }



   /* public String getName()
    {
        return Name;
    }

    public int getAge()
    {
        return Age;
    }

    public String getMobileNum()
    {
        return MobileNum;
    }

    public String getCollege()
    {
        return College;
    }

    public String getDateAndTime()
    {
        return DateAndTime;
    }
*/
   /* public void setName(String name)
    {
        this.Name = name;

    }

    public void setAge(int age)
    {

        this.Age = age;

    }

    public void setMobileNum(String mobileNum)
    {

        this.MobileNum = mobileNum;

    }

    public void setCollege(String college)
    {

        this.College=college;

    }

    public void setDateAndTime(String dateAndTime)
    {
        this.DateAndTime=dateAndTime;
    }
*/

}
