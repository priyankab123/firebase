package com.priyanka.firebasetest;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import java.util.Collections;

import androidx.core.app.NotificationCompat;

public class CreateNotificationChannel extends Application {

        public static final String CHANNEL_1_ID="channel1";
        public static final String CHANNEL_2_ID="channel2";
        public static final String CHANNEL_3_ID="channel3";
        private final int NOTIFICATION_ID=001;



        @Override
        public void onCreate()
        {
            super.onCreate();

            createNotificationChannels();

        }

        private void createNotificationChannels() {



            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            {
                NotificationChannel channel1 =new NotificationChannel(CHANNEL_1_ID,"Channel 1", NotificationManager.IMPORTANCE_DEFAULT);
                NotificationChannel channel2 =new NotificationChannel(CHANNEL_2_ID,"Channel 2", NotificationManager.IMPORTANCE_DEFAULT);
                NotificationChannel channel3 =new NotificationChannel(CHANNEL_3_ID,"Channel 3", NotificationManager.IMPORTANCE_DEFAULT);

                NotificationManager manager =getSystemService(NotificationManager.class);

                manager.createNotificationChannels(Collections.singletonList(channel1));
                manager.createNotificationChannels(Collections.singletonList(channel2));
                manager.createNotificationChannels(Collections.singletonList(channel3));

            }

            // NotificationCompat.Builder builder1= new NotificationCompat.Builder(this,CHANNEL_1_ID);

            NotificationCompat.Builder builder2= new NotificationCompat.Builder(this,CHANNEL_2_ID);

            //NotificationCompat.Builder builder3= new NotificationCompat.Builder(this,CHANNEL_3_ID);



            //NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);
        /*notificationManagerCompat.notify(NOTIFICATION_ID,builder1.build());
        notificationManagerCompat.notify(NOTIFICATION_ID,builder2.build());
        notificationManagerCompat.notify(NOTIFICATION_ID,builder3.build());
*/


        }

    }


