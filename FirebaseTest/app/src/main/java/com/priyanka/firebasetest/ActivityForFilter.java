
        package com.priyanka.firebasetest;

        import androidx.annotation.NonNull;
        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ListView;
        import android.widget.Spinner;
        import android.widget.Toast;

        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.Query;
        import com.google.firebase.database.ValueEventListener;

        import java.io.Serializable;
        import java.util.ArrayList;
        import java.util.List;

public class ActivityForFilter extends AppCompatActivity implements Serializable {

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mstudentReference=mRootReference.child("Students");
    Query query;

    ArrayList<Student> stdList=new ArrayList<Student>();

    ArrayList<StdWithPK> stdWithPKArrayList=new ArrayList<>();

    StdWithPK stdwithpkObject;

    ArrayList<String> pushKeyArrayList=new ArrayList<String>();

    List<String> collegeList;
    String spinnerSelect;

    ArrayAdapter<String> dataAdapter;

    public StudentAdapter adapter;

    ListView listView;
    String key;

    Student student;

    private Spinner spinnerCollege;

    String CollegeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_filter);

        listView=(ListView) findViewById(R.id.simpleListView);

        spinnerCollege=(Spinner) findViewById(R.id.spinnerCollege);
        collegeList=new ArrayList<String>();

              dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, collegeList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCollege.setAdapter(dataAdapter);

        collegeList.add("Select College");

        mstudentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    key= snapshot.getKey();

                    Student student=snapshot.getValue(Student.class);

                    CollegeName=student.College;

                    if(collegeList.contains(CollegeName))
                    {


                    }
                    else {

                        collegeList.add(CollegeName);
                    }

                    Log.d("Coll",student.College);

                }

                dataAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        spinnerCollege.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stdList.clear();
                stdWithPKArrayList.clear();

                if(position==0)
                {
                    mstudentReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                            int ChildCount= (int) dataSnapshot.getChildrenCount();

                            Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                            for (DataSnapshot snapshot:dataSnapshot.getChildren())
                            {
                                key= snapshot.getKey();

                                Student student=snapshot.getValue(Student.class);

                                pushKeyArrayList.add(key);
                                stdList.add(student);

                                // Log.d("ppN",stdwithpkObject.Pk);
                                stdwithpkObject= new StdWithPK(key,student);
                                stdWithPKArrayList.add(stdwithpkObject);


                            }
                            adapter=new StudentAdapter(ActivityForFilter.this,R.layout.activity_for_filter,stdList);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

                stdList.clear();
                stdWithPKArrayList.clear();

                spinnerSelect  = String.valueOf(parent.getItemIdAtPosition(position));

                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + " selected", Toast.LENGTH_SHORT).show();

                if(parent.getItemAtPosition(position).equals("All"))
                {
                    mstudentReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                            int ChildCount= (int) dataSnapshot.getChildrenCount();

                            Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                            for (DataSnapshot snapshot:dataSnapshot.getChildren())
                            {
                                key= snapshot.getKey();

                                Student student=snapshot.getValue(Student.class);

                                pushKeyArrayList.add(key);
                                stdList.add(student);

                                // Log.d("ppN",stdwithpkObject.Pk);
                                stdwithpkObject= new StdWithPK(key,student);
                                stdWithPKArrayList.add(stdwithpkObject);


                            }
                            adapter=new StudentAdapter(ActivityForFilter.this,R.layout.activity_for_filter,stdList);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

                else {
                    mstudentReference.orderByChild("College").equalTo(String.valueOf(parent.getItemAtPosition(position))).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                key = snapshot.getKey();

                                Student student = snapshot.getValue(Student.class);

                                pushKeyArrayList.add(key);
                                stdList.add(student);

                                // Log.d("ppN",stdwithpkObject.Pk);
                                stdwithpkObject= new StdWithPK(key,student);
                                stdWithPKArrayList.add(stdwithpkObject);

                            }

                            adapter = new StudentAdapter(ActivityForFilter.this, R.layout.activity_for_filter, stdList);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                adapter.notifyDataSetChanged();
                Intent intent=new Intent(ActivityForFilter.this,DisplayDetails.class);

                Log.d("pushkey",stdwithpkObject.Std.Name);

                intent.putExtra("position",position);

                intent.putExtra("StdWithPkArrayList",stdWithPKArrayList);

                startActivity(intent);
            }
        });

    }




    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent(ActivityForFilter.this,MainActivity.class);
        startActivity(intent);
        finish();
    }



}