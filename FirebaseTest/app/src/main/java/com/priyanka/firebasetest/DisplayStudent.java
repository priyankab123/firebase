
        package com.priyanka.firebasetest;

        import androidx.annotation.NonNull;
        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ListView;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.Spinner;

        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.Query;
        import com.google.firebase.database.ValueEventListener;

        import java.io.Serializable;
        import java.util.ArrayList;
        import java.util.List;

public class DisplayStudent extends AppCompatActivity implements Serializable {

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mstudentReference=mRootReference.child("Students");
    Query query;

    ArrayList<Student> stdList=new ArrayList<Student>();
    ArrayList ListStd=new ArrayList<Student>();
    ArrayList<String> pushKeyArrayList=new ArrayList<String>();
    ArrayList<StdWithPK> stdWithPKArrayList=new ArrayList<>();
    List<String> collegeList;
    String spinnerSelect;

    int indexOfPushKey;

    Student std;
    StdWithPK stdwithpkObject;

    public StudentAdapter adapter;

    ListView listView;
    String key;

    RadioGroup RG;
    RadioButton rbName,rbCollege;

    Student student;

    private Spinner spinnerCollege;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_student);


        final Bundle bundle =getIntent().getExtras();

        listView=(ListView) findViewById(R.id.simpleListView);
        RG=(RadioGroup) findViewById(R.id.RadioGroup);
        rbName=(RadioButton) findViewById(R.id.sortByName);
        rbCollege=(RadioButton) findViewById(R.id.sortByCollege);

        //spinnerCollege=(Spinner) findViewById(R.id.spinnerCollege);
        /*collegeList=new ArrayList<String>();

        collegeList.add("select college");
        collegeList.add("Vp");
        collegeList.add("Tc");
        collegeList.add("Wadiya");
        collegeList.add("Sp");
        collegeList.add("Fc");
        collegeList.add("All");
*/

/*

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, collegeList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCollege.setAdapter(dataAdapter);
*/




        list();

        //stdList= (ArrayList<Student>) getIntent().getSerializableExtra("arraylist");

        adapter = new StudentAdapter(DisplayStudent.this, R.layout.activity_display_student, stdList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();



        if(stdList==null)
        {
            list();
        }

       /* spinnerCollege.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stdList.clear();
                spinnerSelect  = String.valueOf(parent.getItemIdAtPosition(position));

                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + " selected", Toast.LENGTH_SHORT).show();

                if(parent.getItemAtPosition(position).equals("All"))
                {
                    mstudentReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                            listView.setAdapter(null);


                            int ChildCount= (int) dataSnapshot.getChildrenCount();

                            Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                            for (DataSnapshot snapshot:dataSnapshot.getChildren())
                            {
                                key= snapshot.getKey();

                                Student student=snapshot.getValue(Student.class);

                                pushKeyArrayList.add(key);
                                stdList.add(student);

                                stdwithpkObject= new StdWithPK(key,student);
                                stdWithPKArrayList.add(stdwithpkObject);

                            }

                            adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(adapter);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

                else {
                    mstudentReference.orderByChild("College").equalTo(String.valueOf(parent.getItemAtPosition(position))).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                key = snapshot.getKey();

                                Student student = snapshot.getValue(Student.class);

                                pushKeyArrayList.add(key);
                                stdList.add(student);

                                stdwithpkObject = new StdWithPK(key, student);
                                stdWithPKArrayList.add(stdwithpkObject);

                            }

                            adapter = new StudentAdapter(DisplayStudent.this, R.layout.activity_display_student, stdList);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(adapter);


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
*/
        RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                stdList.clear();
                stdWithPKArrayList.clear();

                switch(checkedId)
                {
                    case R.id.sortByName:
                        mstudentReference.orderByChild("Name").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                                int ChildCount= (int) dataSnapshot.getChildrenCount();

                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));


                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                                {
                                    key= snapshot.getKey();

                                    student=snapshot.getValue(Student.class);
                                    //stdwithpkObject=snapshot.getValue(StdWithPK.class);

                                    pushKeyArrayList.add(key);
                                    stdList.add(student);

                                    // Log.d("ppN",stdwithpkObject.Pk);
                                    stdwithpkObject= new StdWithPK(key,student);
                                    stdWithPKArrayList.add(stdwithpkObject);

                                }

                                adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                                adapter.notifyDataSetChanged();
                                listView.setAdapter(adapter);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        break;


                    case R.id.sortByCollege:


                        mstudentReference.getRef().orderByChild("College").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {


                                int ChildCount= (int) dataSnapshot.getChildrenCount();

                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                                {
                                    key= snapshot.getKey();

                                    student=snapshot.getValue(Student.class);
                                    //stdwithpkObject=snapshot.getValue(StdWithPK.class);

                                    pushKeyArrayList.add(key);
                                    stdList.add(student);

                                    //Log.d("ppC",stdwithpkObject.Pk);

                                    stdwithpkObject= new StdWithPK(key,student);
                                    stdWithPKArrayList.add(stdwithpkObject);

                                }

                                adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                                adapter.notifyDataSetChanged();
                                listView.setAdapter(adapter);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        break;

                    case R.id.sortByAge:

                      /*  Query dataRef = mstudentReference
                                .orderByChild("Age");
*/
                        mstudentReference.orderByChild("Age").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                int ChildCount= (int) dataSnapshot.getChildrenCount();

                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                                {
                                    key= snapshot.getKey();

                                    student=snapshot.getValue(Student.class);
                                    //stdwithpkObject=snapshot.getValue(StdWithPK.class);

                                    pushKeyArrayList.add(key);
                                    stdList.add(student);

                                    // Log.d("ppA",stdwithpkObject.Pk);

                                    stdwithpkObject= new StdWithPK(key,student);
                                    stdWithPKArrayList.add(stdwithpkObject);

                                }

                                adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                                adapter.notifyDataSetChanged();
                                listView.setAdapter(adapter);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        break;

                    case R.id.sortByDate:
                        mstudentReference.orderByChild("DateAndTime").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                                int ChildCount= (int) dataSnapshot.getChildrenCount();

                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));


                                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                                {
                                    key= snapshot.getKey();

                                    student=snapshot.getValue(Student.class);
                                    //stdwithpkObject=snapshot.getValue(StdWithPK.class);

                                    pushKeyArrayList.add(key);
                                    stdList.add(student);

                                    // Log.d("ppN",stdwithpkObject.Pk);
                                    stdwithpkObject= new StdWithPK(key,student);
                                    stdWithPKArrayList.add(stdwithpkObject);

                                }

                                adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                                adapter.notifyDataSetChanged();
                                listView.setAdapter(adapter);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        break;

                }
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                //adapter.notifyDataSetChanged();
                Intent intent=new Intent(DisplayStudent.this,StudentDetails.class);

                Log.d("pushkey",stdwithpkObject.Std.Name);

                intent.putExtra("position",position);

                intent.putExtra("StdWithPkArrayList",stdWithPKArrayList);

                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent(DisplayStudent.this,MainActivity.class);
        startActivity(intent);
        finish();;
    }

    public void list()
    {
        mstudentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int ChildCount= (int) dataSnapshot.getChildrenCount();

                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    key= snapshot.getKey();

                    student=snapshot.getValue(Student.class);
                    //stdwithpkObject=snapshot.getValue(StdWithPK.class);

                    pushKeyArrayList.add(key);
                    stdList.add(student);

                    stdwithpkObject= new StdWithPK(key,student);
                    stdWithPKArrayList.add(stdwithpkObject);

                }

                //adapter=new StudentAdapter(DisplayStudent.this,R.layout.activity_display_student,stdList);
                adapter.notifyDataSetChanged();
                //listView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}

