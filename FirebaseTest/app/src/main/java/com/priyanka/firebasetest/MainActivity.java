package com.priyanka.firebasetest;


//import android.support.v7.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;


import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;


import java.io.Serializable;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.jar.Attributes;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class MainActivity extends AppCompatActivity implements ValueEventListener {

    private TextView headingText,txtcity;
    private EditText name,age,college,mobileNum;
    private RadioButton RBRed,RBBlue,RBGreen;
    private RadioButton styleNormal,styleItalic,styleBold;
    private Button pkButton,subscribeBtn;
    Student std;
    public ArrayList<Student> stdList=new ArrayList<Student>();
    Toast toastName,toastAge,toastCollege,toastMobileNum,toastAllFields;
    private String dateAndtime;
    private String formattedDate;

    Button signOut;

    GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;

   /* private final String CHANNEL_ID="personal notification";
    private final int NOTIFICATION_ID=001;*/
    private NotificationManagerCompat notificationManager;

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mHeadngReference= mRootReference.child("heading");
    private DatabaseReference mFontColorReference=mRootReference.child("fontcolor");
    private DatabaseReference mFontStyeReference=mRootReference.child("fontstyle");
    private DatabaseReference mcityReference=mRootReference.child("city");
    private DatabaseReference mstudentReference=mRootReference.child("Students");

    private DatabaseReference mUserReference=mRootReference.child("Users");

    private DatabaseReference mUserId;

    private DatabaseReference mUserEmail;

    private DatabaseReference mUserSignInCount;

    private DatabaseReference test=mRootReference.child("extra");
    private DatabaseReference pincode=mRootReference.child("Pincode");

    GoogleSignInAccount acct;

    String UserId;
    String UserEmail;


    private FirebaseAuth mAuth ;
    FirebaseUser currentUser;

    FirebaseAuth.AuthStateListener mAuthListener;

    public int counter=1,tempCount;

    public String sample="Modified";

   public String sample2="Modified Second time";

    public String sample3="Modified Second time";


    private  String type;

    String msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("File is modified with this statemnt",sample);

        Log.d("File is modified second time with this statement",sample2);



        notificationManager=NotificationManagerCompat.from(this);


        Toast.makeText(MainActivity.this, "App started",
                Toast.LENGTH_SHORT).show();

        name=(EditText) findViewById(R.id.editName);
        age=(EditText) findViewById(R.id.editAge);
        college=(EditText) findViewById(R.id.editCollege);
        mobileNum=(EditText) findViewById(R.id.editPhonenum);

        dateAndtime= DateFormat.getDateTimeInstance().format(new Date());

        pkButton=(Button) findViewById(R.id.pkButton);
        subscribeBtn=(Button) findViewById(R.id.subscribe);


        FirebaseMessaging.getInstance().subscribeToTopic("Student_Notification");
                /*.addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            msg = "msg_subscribe_failed";
                        }
                        else
                        {
                            msg = "msg_subscribed";

                        }
                        Log.d("Message", msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
*/

        mcityReference.removeValue();
        pincode.removeValue();

        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;

        toastAllFields = Toast.makeText(context, R.string.allfields, duration);

        signOut=(Button) findViewById(R.id.signOut);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("710663335759-ul2bhng7kmth656js4eeg28u5jnli134.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        acct= GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        FirebaseAuth auth = FirebaseAuth.getInstance();

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        Log.d("UUU",currentUser.getUid());

         mUserId=mUserReference.child(currentUser.getUid());

         mUserEmail=mUserId.child("Email");

        mUserSignInCount=mUserId.child("SignInCounter");




        mUserReference.child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int ChildCount = (int) dataSnapshot.getChildrenCount();

                String key = dataSnapshot.getKey();

                Log.d("KEY", key);

                    if(dataSnapshot.hasChild("SignInCounter"))
                    {
                       Integer valuobj = (int) dataSnapshot.child("SignInCounter").getValue(Integer.class);

                        Log.d("valobj",valuobj.toString());

                        tempCount = valuobj + 1;

                        Log.d("tempobj", String.valueOf(tempCount));

                        mUserSignInCount.setValue(tempCount);
                    }
                else
                    {
                        mUserSignInCount.setValue(counter);
                    }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });



        mUserEmail.setValue(currentUser.getEmail());





        GoogleSignInAccount acct= (GoogleSignInAccount) GoogleSignIn.getLastSignedInAccount(getApplicationContext());


        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signOut();
                Intent intent=new Intent(MainActivity.this,SignIn.class);
                startActivity(intent);


            }
        });


        mcityReference.addValueEventListener( new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue(String.class) != null) {
                    String city = dataSnapshot.getValue(String.class);
                    txtcity.setText(city);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });


        mstudentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int ChildCount= (int) dataSnapshot.getChildrenCount();

                Log.d("CHILDREN_COUNT", String.valueOf(ChildCount));

                //String pushkey = mstudentReference.push().getKey();

                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    String key= snapshot.getKey();

                    Student student=snapshot.getValue(Student.class);

                   stdList.add(student);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mRootReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int ChildCount= (int) dataSnapshot.getChildrenCount();


                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    String key= snapshot.getKey();

                    String value=snapshot.getValue().toString();

                    Log.d("KEY_NAME",key);

                    if(key.equals("heading") || key.equals("fontcolor") || key.equals("fontstyle") )
                    {
                        Log.d("Values",value);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @SuppressLint("WrongConstant")
    public void onClickPushKey(View view) {

            String Name = name.getText().toString();
            String Age= age.getText().toString();
            String MobileNum=mobileNum.getText().toString();
            String College=college.getText().toString();



        if(Name.matches("") && Age.matches("") && MobileNum.matches("") && College.matches(""))
        {
           toastAllFields.show();
            name.setError("please fill name");
            college.setError("please fill college");
            age.setError("please fill age");
            mobileNum.setError("please fill mobile number");

        }

        else if(Name.matches("") )
        {
            name.setError("please fill name");
        }

        else if( College.matches(""))
        {
            college.setError("please fill college");
        }

        else if( Age.matches("") )
        {
            age.setError("please fill age");

        }
        else if( MobileNum.matches("") )
        {
            mobileNum.setError("please fill mobile number");

        }

        else if(MobileNum.length()<10 || MobileNum.length()>10)
        {
            Toast.makeText(MainActivity.this, "Mobile Number should 10 digits",
                    Toast.LENGTH_SHORT).show();

        }

        else {

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            formattedDate = df.format(c.getTime());
            Log.d("DATE",formattedDate);


            std=new Student(Name,Integer.parseInt(Age),MobileNum,College,formattedDate);

            stdList.add(std);

            String pushkey = mRootReference.push().getKey();
                mRootReference.child("Students").child(pushkey).setValue(std);

            }

            name.setText("");
            college.setText("");
            age.setText("");
            mobileNum.setText("");


       /* NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentTitle("notification for added student")
                .setAutoCancel(true)
                .setColorized(true)
                .setColor(0xff0000ff)
                .setContentText("One student is added in database")
                .setPriority(NotificationCompat.PRIORITY_HIGH);




        Intent resultIntent = new Intent(this, DisplayStudent.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notification.setContentIntent(resultPendingIntent);

        int mNotificationId = 001;

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, notification.build());*/

    }


    public void onClickDisplay(View view) {

        Intent intent=new Intent(MainActivity.this,DisplayStudent.class);

        Bundle bundle=new Bundle();
        Bundle args = new Bundle();

        intent.putExtra("arraylist",(Serializable) stdList);
        intent.putExtra( "BUNDLE", args );
        intent.putExtra("stdObject", (Serializable)std);
        startActivity(intent);
    }



    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onStart()
    {
        super.onStart();


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(MainActivity.this,SignIn.class);
        startActivity(intent);

    }

    public void onClickDisplay2(View view) {

        Intent intent=new Intent(MainActivity.this,ActivityForFilter.class);
        startActivity(intent);
    }


    public void signOut()
    {
       mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
           @Override
           public void onComplete(@NonNull Task<Void> task) {
             Toast.makeText(MainActivity.this,"SIGN OUT",Toast.LENGTH_SHORT).show();


           }
       });
    }


}