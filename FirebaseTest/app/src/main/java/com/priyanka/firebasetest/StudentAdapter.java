package com.priyanka.firebasetest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends BaseAdapter implements Serializable {

    Context context;
   private  ArrayList<Student> students;
    //Button delete;
    Student obj;

    String s1;

    private ArrayList<Integer> hiddenPositions = new ArrayList<>();
    ArrayList<String> listItems;



    public StudentAdapter(Activity context, int listview, ArrayList students) {
        this.context = context;
        this.students  = (ArrayList<Student>) students;


    }

    @Override
    public int getCount() {
        return students.size()-hiddenPositions.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return students.indexOf(getItem(position));
    }

    public void removeItem(Object pos) {

        students.remove(pos);

    }

    private static class ViewHolder
    {
        public ListView simpleList;
        public TextView textName;
        public TextView textAge;
        public TextView textMobileNum;
        public TextView textCollege;
        public TextView textDate;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;


        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.listview, null);
        }
            Student rssItem = (Student) getItem(position);


            if (rssItem != null) {
                holder = new ViewHolder();


                holder.textName = (TextView) convertView.findViewById(R.id.textName);
                holder.textAge = (TextView) convertView.findViewById(R.id.textAge);
                holder.textMobileNum = (TextView) convertView.findViewById(R.id.textMoileNum);
                holder.textCollege = (TextView) convertView.findViewById(R.id.textCollege);
                holder.textDate=(TextView) convertView.findViewById(R.id.textDateAndTime);

                holder.simpleList = (ListView) convertView.findViewById(R.id.simpleListView);


                Student std_pos = students.get(position);


                holder.textName.setText(std_pos.Name);
                holder.textAge.setText(String.valueOf(std_pos.Age));
                holder.textMobileNum.setText(std_pos.MobileNum);
                holder.textCollege.setText(std_pos.College);
                holder.textDate.setText(std_pos.DateAndTime);

                for (Integer hiddenIndex : hiddenPositions) {
                    if (hiddenIndex <= position) {
                        position = position + 1;
                    }
                }
                //String testString = listItems.get(position);

          /* if(std_pos.getCollege().equals("vp"))
           {
               holder.simpleList.setBackgroundColor(Color.GREEN);
           }
           else
           {
               holder.simpleList.setBackgroundColor(Color.GRAY);
           }*/



            }

        /*final Student stud=students.get(position);

        if((stud.College).equals("vp"))
        {
            convertView.setBackgroundColor(Color.GREEN);
        }
        else if((stud.College).equals("wadiya"))
        {
            convertView.setBackgroundColor(Color.YELLOW);
        }
        else if((stud.College).equals("tc"))
        {
            convertView.setBackgroundColor(Color.BLUE);
        }
        else
        {
            convertView.setBackgroundColor(Color.RED);
        }*/



        return convertView;
    }


    public void updateData(ArrayList<Student> updatedData) {
        students = updatedData;
        this.notifyDataSetChanged();
    }

    public void notify(ArrayList<Student> list){
        students= list;
        this.notifyDataSetChanged();
    }


}