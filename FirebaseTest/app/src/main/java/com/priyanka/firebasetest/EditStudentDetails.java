package com.priyanka.firebasetest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

public class EditStudentDetails extends AppCompatActivity implements Serializable {

    private NotificationManagerCompat notificationManager;

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mstudentReference=mRootReference.child("Students");
    private DatabaseReference mChildRef;


    Student stdObj;
    String pushKeyStr;
    String name,college,mobileNum;
    int age;
    String dateAndtime;

    EditText edtName,edtAge,edtCollege,edtMobile;

    Toast toastAllFields;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student_details);

        notificationManager=NotificationManagerCompat.from(this);


        Bundle bundle =getIntent().getExtras();

        pushKeyStr=bundle.getString("PUSHKEY");

        stdObj=(Student) bundle.getSerializable("OBJ");

        mChildRef=mstudentReference.child(pushKeyStr);

        name=stdObj.Name;
        age= stdObj.Age;
        college=stdObj.College;
        mobileNum=stdObj.MobileNum;




        //dateAndtime= stdObj.DateAndTime;


        edtName=(EditText) findViewById(R.id.editName);
        edtAge=(EditText) findViewById(R.id.editAge);
        edtCollege=(EditText) findViewById(R.id.editCollege);
        edtMobile=(EditText) findViewById(R.id.editPhoneNum);

        edtName.setText(name);
        edtAge.setText(""+age);
        edtCollege.setText(college);
        edtMobile.setText(mobileNum);

        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;

        toastAllFields = Toast.makeText(context, R.string.allfields, duration);




    }

    public void onClicksave(View view) {

                    String nameAfterEdit= String.valueOf(edtName.getText());
                    String collegeAfterEdit= String.valueOf(edtCollege.getText());
                    String ageAfterEdit= String.valueOf(edtAge.getText());
                    String mobileNumAfterEdit= String.valueOf(edtMobile.getText());

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    dateAndtime = df.format(c.getTime());



        if( (nameAfterEdit.matches("")) && (collegeAfterEdit.matches("")) && (mobileNumAfterEdit.matches("")) && ageAfterEdit.matches(""))
                     {
                        toastAllFields.show();
                        edtName.setError("please fill name");
                         edtAge.setError("please fill age");
                        edtMobile.setError("please fill mobile number");
                         edtCollege.setError("please fill college");
                     }

                     else if(nameAfterEdit.matches("") )
                     {

                         edtName.setError("please fill name");

                     }

                     else if(ageAfterEdit.matches(""))
                     {

                         edtAge.setError("please fill age");

                     }

                    else if((collegeAfterEdit.matches("")) ) {

                         edtCollege.setError("please fill College");
                    }

                    else if((mobileNumAfterEdit.matches("")) ) {

                         edtMobile.setError("please fill mobile number");
                    }



                  else
                    {
                        Student std = new Student(nameAfterEdit, Integer.parseInt(ageAfterEdit), mobileNumAfterEdit, collegeAfterEdit,dateAndtime);
                        mChildRef.setValue(std);

                        Toast.makeText(EditStudentDetails.this, "Information Edited successfully",
                                Toast.LENGTH_SHORT).show();




                     /*   NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_2_ID)
                                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                                .setColorized(true)
                                .setColor(0xff0000ff)
                                .setContentTitle("notification for Edited")
                                .setOngoing(true)
                                .setAutoCancel(true)
                                .setContentText("One student's information  edited successfully")
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


                        Intent resultIntent = new Intent(this, MainActivity.class);

                        PendingIntent resultPendingIntent =
                                PendingIntent.getActivity(
                                        this,
                                        0,
                                        resultIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT
                                );

                        notification.setContentIntent(resultPendingIntent);

                        int mNotificationId = 001;

                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                        mNotifyMgr.notify(mNotificationId, notification.build());

*/

                        //notificationManager.notify(1,notification.build());


                        Intent intent = new Intent(EditStudentDetails.this, DisplayStudent.class);
                        startActivity(intent);
                    }






    }

    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent(EditStudentDetails.this,DisplayStudent.class);
        startActivity(intent);
        finish();

    }

}
