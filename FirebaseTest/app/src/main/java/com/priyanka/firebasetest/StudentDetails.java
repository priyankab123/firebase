package com.priyanka.firebasetest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class StudentDetails extends AppCompatActivity implements Serializable {

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mstudentReference=mRootReference.child("Students");
    private DatabaseReference mStudentPushKeyReference;

    private NotificationManagerCompat notificationManager;


    private final int NOTIFICATION_ID=001;


    ArrayList<Student> stdArrayList;
    TextView name,college,age,phoneNumber,pushkey,dateTime;
    ArrayList<StdWithPK> stdWithPKArrayList;
    StdWithPK stdWithPK;

    Button removeBtn;
    String Name;
    String College;
    String PhoneNum;
    int Age;
    public String pushKey;
    public Student std;
    String dateAndTime;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);

        notificationManager=NotificationManagerCompat.from(this);


        stdWithPKArrayList=(ArrayList<StdWithPK>) getIntent().getSerializableExtra(
                "StdWithPkArrayList");

        Bundle bundle =getIntent().getExtras();

        position = bundle.getInt("position");

        String nnn= (String) getIntent().getSerializableExtra("Nn");

        Log.d("POSITION", String.valueOf(position));

        stdWithPK=stdWithPKArrayList.get(position);
        pushKey=stdWithPK.Pk;

        std=stdWithPK.Std;

        pushkey=(TextView) findViewById(R.id.pushKey);
        name=(TextView) findViewById(R.id.studentName);
        college=(TextView) findViewById(R.id.studentCollege);
        age=(TextView) findViewById(R.id.studentAge);
        phoneNumber=(TextView) findViewById(R.id.studentPhoneNum);
        removeBtn=(Button) findViewById(R.id.removeStudent);
        dateTime=(TextView) findViewById(R.id.dateAndTime);

        Name=std.Name;
        College=std.College;
        PhoneNum=std.MobileNum;
        Age=std.Age;
        dateAndTime=std.DateAndTime;

        Log.d("NNNNNN",Name);

        name.setText(Name);
        college.setText(College);
        phoneNumber.setText(PhoneNum);
        age.setText(""+Age);

       /* if(dateAndTime.isEmpty())
        {
            dateTime.setText("Not Added");
        }
        else {*/
            dateTime.setText(dateAndTime);
       // }

       // Log.d("DATE",dateAndTime);




        pushkey.setText(pushKey);


        mStudentPushKeyReference=mstudentReference.child(pushKey);

       /* mstudentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int counter=0;
                for (DataSnapshot child : dataSnapshot.getChildren()) {

                    if(counter==position) {
                        String key = child.getKey();
                        pushkey.setText(key);
                        break;
                    }
                    counter++;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

    }

    public void onRemoveClick(View view) {
        mStudentPushKeyReference.removeValue();

      /*  NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_3_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentTitle("notification for deleted")
                .setAutoCancel(true)
                .setColorized(true)
                .setColor(0xff0000ff)
                .setContentText("One student information is deleted from database")
                .setPriority(NotificationCompat.PRIORITY_HIGH);



        Intent resultIntent = new Intent(this, DisplayStudent.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notification.setContentIntent(resultPendingIntent);

        int mNotificationId = 001;

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, notification.build());
*/

        //notificationManager.notify(1,notification);


        Intent intent=new Intent(StudentDetails.this,DisplayStudent.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent(StudentDetails.this,DisplayStudent.class);
        startActivity(intent);
        finish();

    }

    public void onClickEdit(View view) {

        Intent intent =new Intent(StudentDetails.this,EditStudentDetails.class);
        intent.putExtra("PUSHKEY",pushKey);
        intent.putExtra("OBJ", (Serializable) std);

        startActivity(intent);
    }
}
