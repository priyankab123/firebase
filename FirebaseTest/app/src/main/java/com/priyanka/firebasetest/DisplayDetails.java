package com.priyanka.firebasetest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class DisplayDetails extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference=firebaseDatabase.getReference();
    private DatabaseReference mstudentReference=mRootReference.child("Students");
    private DatabaseReference mStudentPushKeyReference;

    private NotificationManagerCompat notificationManager;


    private final int NOTIFICATION_ID=001;


    ArrayList<Student> stdArrayList;
    TextView name,college,age,phoneNumber,pushkey,dateTime;
    ArrayList<StdWithPK> stdWithPKArrayList;
    StdWithPK stdWithPK;

    Button removeBtn;
    String Name;
    String College;
    String PhoneNum;
    int Age;
    String pushKey;
    Student std;
    String dateAndTime;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_details);

        stdWithPKArrayList=(ArrayList<StdWithPK>) getIntent().getSerializableExtra(
                "StdWithPkArrayList");

        Bundle bundle =getIntent().getExtras();

        position = bundle.getInt("position");

        String nnn= (String) getIntent().getSerializableExtra("Nn");

        Log.d("POSITION", String.valueOf(position));

        stdWithPK=stdWithPKArrayList.get(position);
        pushKey=stdWithPK.Pk;

        std=stdWithPK.Std;

        pushkey=(TextView) findViewById(R.id.pushKey);
        name=(TextView) findViewById(R.id.studentName);
        college=(TextView) findViewById(R.id.studentCollege);
        age=(TextView) findViewById(R.id.studentAge);
        phoneNumber=(TextView) findViewById(R.id.studentPhoneNum);
        removeBtn=(Button) findViewById(R.id.removeStudent);
        dateTime=(TextView) findViewById(R.id.dateAndTime);

        Name=std.Name;
        College=std.College;
        PhoneNum=std.MobileNum;
        Age=std.Age;
        dateAndTime=std.DateAndTime;

        Log.d("NNNNNN",Name);

        name.setText(Name);
        college.setText(College);
        phoneNumber.setText(PhoneNum);
        age.setText(""+Age);

       /* if(dateAndTime.isEmpty())
        {
            dateTime.setText("Not Added");
        }
        else {*/
        dateTime.setText(dateAndTime);
        // }

        // Log.d("DATE",dateAndTime);




        pushkey.setText(pushKey);



    }
}
