package com.priyanka.firebasetest;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import androidx.core.app.NotificationCompat;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private  String type,Name,MobileNum,college;

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage)
    {
        if (remoteMessage.getData().size() > 0)
        {
            //type = remoteMessage.getData().get("Name");
            //Log.d("type", type);

           /* if(type.equals("Name"))
            {
                Name = remoteMessage.getData().get("Name");

                Log.wtf("Name",Name);

                Notification(Name);
            }*/
            Name = remoteMessage.getData().get("name");
            MobileNum=remoteMessage.getData().get("MobileNum");
            college=remoteMessage.getData().get("College");

            type = remoteMessage.getData().get("type");
            Log.d("type", type);

            if(type.equals("Add_Stud")) {

                AddNotification(Name, MobileNum, college);
            }
            else if(type.equals("Delete_Stud"))
            {
                DeleteNotification(Name,MobileNum,college);
            }
            else if(type.equals("Update_Stud"))
            {
                UpdateNotification(Name,MobileNum,college);
            }

        }
    }


    private void AddNotification(String Name,String mobileNum,String college)
    {

        NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentTitle("Student Added")
                .setAutoCancel(true)
                .setColorized(true)
                .setColor(0xff0000ff)
                .setContentText(Name +"  , " +mobileNum +" , " +college)
                .setPriority(NotificationCompat.PRIORITY_HIGH);


        Intent resultIntent = new Intent(this, DisplayStudent.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notification.setContentIntent(resultPendingIntent);

        int mNotificationId = 001;

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, notification.build());
    }


    private void DeleteNotification(String Name,String mobileNum,String college)
    {

        NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_3_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentTitle("Student deleted")
                .setAutoCancel(true)
                .setColorized(true)
                .setColor(0xff0000ff)
                .setContentText(Name +"  , " +mobileNum +" , " +college)
                .setPriority(NotificationCompat.PRIORITY_HIGH);



        Intent resultIntent = new Intent(this, DisplayStudent.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notification.setContentIntent(resultPendingIntent);

        int mNotificationId = 001;

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, notification.build());

    }



    private void UpdateNotification(String Name,String mobileNum,String college)
    {
        NotificationCompat.Builder notification= new NotificationCompat.Builder(this , CreateNotificationChannel.CHANNEL_2_ID)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setColorized(true)
                .setColor(0xff0000ff)
                .setContentTitle("Student Updated")
                .setOngoing(true)
                .setAutoCancel(true)
                .setContentText(Name +"  , " +mobileNum +" , " +college)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


        Intent resultIntent = new Intent(this, DisplayStudent.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notification.setContentIntent(resultPendingIntent);

        int mNotificationId = 001;

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, notification.build());

    }



}
